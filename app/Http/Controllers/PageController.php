<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class PageController extends Controller
{
    public function index()
    {
        return Inertia::render('Index', [
            'title' => env('APP_NAME') . ' | Home',
            'message' => 'Hi, I just learn laravel-inertia'
        ]);
    }
    
    public function about()
    {
        return Inertia::render('About', [
            'title' => env('APP_NAME') . ' | About',
            'message' => 'This is the about page'
        ]);
    }
}
